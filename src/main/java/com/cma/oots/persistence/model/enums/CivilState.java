package com.cma.oots.persistence.model.enums;

public enum CivilState {
	SINGLE("Soltero / Consensual"),
	IN_RELATIONSHIP("Soltero / Noviazgo"),
	MARRIED("Casado"),
	DIVORCED("Divorciado"),
	SEPARATED("Separado"),
	RE_MARRIED("2do Matrimonio"),
	WIDOW("Viudo");
	
	private String desc;
	CivilState(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
