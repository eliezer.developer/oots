package com.cma.oots.persistence.model.enums;

public enum Position {
	SUPERVISOR("Supervisor"),
	SOCIAL_WORKER("Trabajadora Social"),
	ADMINISTRATIVE_ASSITANT("Asistente Administrativa"),
	PSYCOLOGIS("Psycologa");
	
	private String desc;
	Position(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
