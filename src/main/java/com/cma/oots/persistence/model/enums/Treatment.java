package com.cma.oots.persistence.model.enums;

public enum Treatment {
	YES("Sí"),
	NO("No"),
	UNKNOW("Se desconoce");
	
	private String desc;
	Treatment(String desc) {
        this.desc=desc;
    }

    public String getDesc() {
        return desc;
    }
}
