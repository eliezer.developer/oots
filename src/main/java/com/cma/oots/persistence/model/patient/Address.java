package com.cma.oots.persistence.model.patient;

public class Address {
 private String firstLine;
 private String secondLine;
 private String state;
 private String country;
 private String zipcode;
 
public String getFirstLine() {
	return firstLine;
}
public void setFirstLine(String firstLine) {
	this.firstLine = firstLine;
}
public String getSecondLine() {
	return secondLine;
}
public void setSecondLine(String secondLine) {
	this.secondLine = secondLine;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getZipcode() {
	return zipcode;
}
public void setZipcode(String zipcode) {
	this.zipcode = zipcode;
}
}
