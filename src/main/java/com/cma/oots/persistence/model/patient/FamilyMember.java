package com.cma.oots.persistence.model.patient;

import com.cma.oots.persistence.model.enums.Scholarity;

public class FamilyMember {
	private Person person;
	private int age;
	private String relationship;
	private Scholarity scholarity;
	private String occupation;
	
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public Scholarity getScholarity() {
		return scholarity;
	}
	public void setScholarity(Scholarity scholarity) {
		this.scholarity = scholarity;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
}
