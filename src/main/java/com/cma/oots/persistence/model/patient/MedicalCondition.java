package com.cma.oots.persistence.model.patient;

import com.cma.oots.persistence.model.enums.Treatment;

public class MedicalCondition {
	private String condition;
	private Treatment treatment;
	
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public Treatment getTreatment() {
		return treatment;
	}
	public void setTreatment(Treatment treatment) {
		this.treatment = treatment;
	}
}
