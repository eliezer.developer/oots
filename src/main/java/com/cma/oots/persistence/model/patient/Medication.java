package com.cma.oots.persistence.model.patient;

public class Medication {
	private String name;
	private String amount;
	private String recurrence;
	private MedicalCondition condition;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRecurrence() {
		return recurrence;
	}
	public void setRecurrence(String recurrence) {
		this.recurrence = recurrence;
	}
	public MedicalCondition getCondition() {
		return condition;
	}
	public void setCondition(MedicalCondition condition) {
		this.condition = condition;
	}
}
