package com.cma.oots.persistence.model.patient;

import java.util.Date;
import java.util.List;

import com.cma.oots.persistence.model.enums.CivilState;
import com.cma.oots.persistence.model.enums.Gender;
import com.cma.oots.persistence.model.enums.Medicare;
import com.cma.oots.persistence.model.enums.Scholarity;

public class Patient {
	private Long id;
	private Person person;
	private String nickname;
	private Date birthdate;
	private Gender gender;
	private CivilState civilState;
	private String religion;
	private Person tutor;
	
	private Scholarity scholarity;
	private String otherScholarity;
	private String school;
	private String profession;
	private String placeOfWork;
	private String medicalInsurance;
	private Medicare medicare;
	private String medicareComplementary;
	private Date insuranceExpiration;
	private Person referral;
	private String otherServices;
	
	private String incomeSalary;
	private String incomeSocialSecurity;
	private String incomePension;
	private String incomeEBT;
	private String incomeChildSupport;
	private String incomeWellness;
	private String incomeRent;
	private String incomeUnemployment;
	private String incomeOwnBusiness;
	private String incomeOther;
	
	private Person emergencyContact;
	
	private List<FamilyMember> familyMembers;
	
	private List<MedicalCondition> physicalConditions;
	private List<String> physicalPaternalFamilyHistory;
	private List<String> physicalMaternalFamilyHistory;
	
	private boolean hasBeenHospitalized;
	private int hospitalizationCount;
	private List<DateReason> hospitalizationList;
	
	private boolean hasBeenOperated;
	private int operationCount;
	private List<DateReason> operationList;
	
	private List<MedicalCondition> psychiatricConditions;
	private List<String> psychiatricPaternalFamilyHistory;
	private List<String> psychiatricMaternalFamilyHistory;
	
	private List<PsychiatricTreatment> psychiatricTreatment;
	
	private List<Medication> medication;
	
	private boolean hasBeenHospitalizedPsychiatrically;
	private int psychiatricHospitalizationCount;
	private List<DateReason> psychiatricHospitalizationList;
}
