package com.cma.oots.persistence.model.patient;

public class PsychiatricTreatment {
	private Person doctor;
	private String specialty;
	
	public Person getDoctor() {
		return doctor;
	}
	public void setDoctor(Person doctor) {
		this.doctor = doctor;
	}
	public String getSpecialty() {
		return specialty;
	}
	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}
}
