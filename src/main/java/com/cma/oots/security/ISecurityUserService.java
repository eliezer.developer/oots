package com.cma.oots.security;

public interface ISecurityUserService {

    String validatePasswordResetToken(long id, String token);

}
