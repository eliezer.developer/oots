package com.cma.oots.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.cma.oots.persistence" })
@EnableJpaRepositories(basePackages = "com.cma.oots.persistence.dao")
public class PersistenceJPAConfig {

    public PersistenceJPAConfig() {
        super();
    }
}
