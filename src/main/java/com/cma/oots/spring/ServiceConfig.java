package com.cma.oots.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.cma.oots.service" })
public class ServiceConfig {
}
