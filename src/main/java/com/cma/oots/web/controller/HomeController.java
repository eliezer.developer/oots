package com.cma.oots.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.cma.oots.web.dto.PatientDto;

@Controller
public class HomeController {
	
	@GetMapping("/home")
    public String oots(Model model) {
    	model.addAttribute("interview", new PatientDto());
    	model.addAttribute("home_Active", true);
        return "home";
    }
	
	@GetMapping("/test")
    public String test(Model model) {
		model.addAttribute("test_Active", true);
        return "test";
    }
}
