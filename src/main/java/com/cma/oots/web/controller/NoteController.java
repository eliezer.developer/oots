package com.cma.oots.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.cma.oots.web.dto.PatientDto;

@Controller
public class NoteController {
	
	@GetMapping("/initialInterview")
    public String initialInterview(Model model) {
    	model.addAttribute("interview", new PatientDto());
        return "note/initialInterview";
    }
	
	@GetMapping("/progressNote")
    public String progressNote(Model model) {
    	model.addAttribute("interview", new PatientDto());
        return "note/progressNote";
    }
	
	@GetMapping("/servicePlan")
    public String servicePlan(Model model) {
    	model.addAttribute("interview", new PatientDto());
        return "note/servicePlan";
    }
	
	@GetMapping("/weighing")
    public String weighing(Model model) {
    	model.addAttribute("interview", new PatientDto());
        return "note/weighing";
    }
	
	@GetMapping("/closingReport")
    public String closingReport(Model model) {
    	model.addAttribute("interview", new PatientDto());
        return "note/closingReport";
    }
	
	@GetMapping("/transfer")
    public String transfer(Model model) {
    	model.addAttribute("interview", new PatientDto());
        return "note/transfer";
    }
	
	@GetMapping("/discussion")
    public String discussion(Model model) {
    	model.addAttribute("interview", new PatientDto());
        return "note/discussion";
    }
}
