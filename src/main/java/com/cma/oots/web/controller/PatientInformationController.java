package com.cma.oots.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.cma.oots.web.dto.PatientDto;

@Controller
public class PatientInformationController {

	@GetMapping("/patient")
    public String oots(Model model) {
    	model.addAttribute("interview", new PatientDto()); 
    	model.addAttribute("interview_Active", true);
        return "patient/index";
    }
	
	@PostMapping("/patient")
	public String post(Model model) {
    	model.addAttribute("interview", new PatientDto()); 
    	model.addAttribute("interview_Active", true);
        return "patient/index";
    }
	
}
