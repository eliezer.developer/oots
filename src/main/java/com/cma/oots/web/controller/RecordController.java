package com.cma.oots.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.cma.oots.web.dto.PatientDto;

@Controller
public class RecordController {
	
	@GetMapping("/record")
    public String oots(Model model) {
    	model.addAttribute("record", new PatientDto()); 
    	model.addAttribute("record_Active", true);
        return "record/index";
    }
}
