package com.cma.oots.web.dto;

import java.util.Date;

import com.cma.oots.persistence.model.enums.CivilState;
import com.cma.oots.persistence.model.enums.Gender;
import com.cma.oots.persistence.model.enums.Medicare;
import com.cma.oots.persistence.model.enums.Scholarity;

public class PatientDto {
	private String name;
	private String nickname;
	private Date birthdate;
	private Gender gender;
	private CivilState civilState;
	private String religion;
	private String tutorName;
	private String tutorPhone;
	private String mainPhone;
	private String mobilePhone;
	private String workPhone;
	private String email;
	private String postalAddress;
	private String postalState;
	private String postalCountry;
	private String postalZipcode;
	private String physicalAddress;
	private String physicalState;
	private String physicalCountry;
	private String physicalZipcode;
	private Scholarity scholarity;
	private String otherScholarity;
	private String school;
	private String profession;
	private String placeOfWork;
	private String medicalInsurance;
	private Medicare medicare;
	private String medicareComplementary;
	private Date insuranceExpiration;
	private String referralName;
	private String referralPhone;
	private String otherServices;
	
	private String incomeSalary;
	private String incomeSocialSecurity;
	private String incomePension;
	private String incomeEBT;
	private String incomeChildSupport;
	private String incomeWellness;
	private String incomeRent;
	private String incomeUnemployment;
	private String incomeOwnBusiness;
	private String incomeOther;
	
	private String ecName;
	private String ecRelationship;
	private String ecAddress;
	private String ecMainPhone;
	private String ecMobilePhone;
	private String ecWorkPhone;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public CivilState getCivilState() {
		return civilState;
	}
	public void setCivilState(CivilState civilState) {
		this.civilState = civilState;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getTutorName() {
		return tutorName;
	}
	public void setTutorName(String tutorName) {
		this.tutorName = tutorName;
	}
	public String getTutorPhone() {
		return tutorPhone;
	}
	public void setTutorPhone(String tutorPhone) {
		this.tutorPhone = tutorPhone;
	}
	public String getMainPhone() {
		return mainPhone;
	}
	public void setMainPhone(String mainPhone) {
		this.mainPhone = mainPhone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	public String getPostalState() {
		return postalState;
	}
	public void setPostalState(String postalState) {
		this.postalState = postalState;
	}
	public String getPostalCountry() {
		return postalCountry;
	}
	public void setPostalCountry(String postalCountry) {
		this.postalCountry = postalCountry;
	}
	public String getPostalZipcode() {
		return postalZipcode;
	}
	public void setPostalZipcode(String postalZipcode) {
		this.postalZipcode = postalZipcode;
	}
	public String getPhysicalAddress() {
		return physicalAddress;
	}
	public void setPhysicalAddress(String physicalAddress) {
		this.physicalAddress = physicalAddress;
	}
	public String getPhysicalState() {
		return physicalState;
	}
	public void setPhysicalState(String physicalState) {
		this.physicalState = physicalState;
	}
	public String getPhysicalCountry() {
		return physicalCountry;
	}
	public void setPhysicalCountry(String physicalCountry) {
		this.physicalCountry = physicalCountry;
	}
	public String getPhysicalZipcode() {
		return physicalZipcode;
	}
	public void setPhysicalZipcode(String physicalZipcode) {
		this.physicalZipcode = physicalZipcode;
	}
	public Scholarity getScholarity() {
		return scholarity;
	}
	public void setScholarity(Scholarity scholarity) {
		this.scholarity = scholarity;
	}
	public String getOtherScholarity() {
		return otherScholarity;
	}
	public void setOtherScholarity(String otherScholarity) {
		this.otherScholarity = otherScholarity;
	}
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	public String getProfession() {
		return profession;
	}
	public void setProfession(String profession) {
		this.profession = profession;
	}
	public String getPlaceOfWork() {
		return placeOfWork;
	}
	public void setPlaceOfWork(String placeOfWork) {
		this.placeOfWork = placeOfWork;
	}
	public String getMedicalInsurance() {
		return medicalInsurance;
	}
	public void setMedicalInsurance(String medicalInsurance) {
		this.medicalInsurance = medicalInsurance;
	}
	public Medicare getMedicare() {
		return medicare;
	}
	public void setMedicare(Medicare medicare) {
		this.medicare = medicare;
	}
	public String getMedicareComplementary() {
		return medicareComplementary;
	}
	public void setMedicareComplementary(String medicareComplementary) {
		this.medicareComplementary = medicareComplementary;
	}
	public Date getInsuranceExpiration() {
		return insuranceExpiration;
	}
	public void setInsuranceExpiration(Date insuranceExpiration) {
		this.insuranceExpiration = insuranceExpiration;
	}
	public String getReferralName() {
		return referralName;
	}
	public void setReferralName(String referralName) {
		this.referralName = referralName;
	}
	public String getReferralPhone() {
		return referralPhone;
	}
	public void setReferralPhone(String referralPhone) {
		this.referralPhone = referralPhone;
	}
	public String getOtherServices() {
		return otherServices;
	}
	public void setOtherServices(String otherServices) {
		this.otherServices = otherServices;
	}
	public String getIncomeSalary() {
		return incomeSalary;
	}
	public void setIncomeSalary(String incomeSalary) {
		this.incomeSalary = incomeSalary;
	}
	public String getIncomeSocialSecurity() {
		return incomeSocialSecurity;
	}
	public void setIncomeSocialSecurity(String incomeSocialSecurity) {
		this.incomeSocialSecurity = incomeSocialSecurity;
	}
	public String getIncomePension() {
		return incomePension;
	}
	public void setIncomePension(String incomePension) {
		this.incomePension = incomePension;
	}
	public String getIncomeEBT() {
		return incomeEBT;
	}
	public void setIncomeEBT(String incomeEBT) {
		this.incomeEBT = incomeEBT;
	}
	public String getIncomeChildSupport() {
		return incomeChildSupport;
	}
	public void setIncomeChildSupport(String incomeChildSupport) {
		this.incomeChildSupport = incomeChildSupport;
	}
	public String getIncomeWellness() {
		return incomeWellness;
	}
	public void setIncomeWellness(String incomeWellness) {
		this.incomeWellness = incomeWellness;
	}
	public String getIncomeRent() {
		return incomeRent;
	}
	public void setIncomeRent(String incomeRent) {
		this.incomeRent = incomeRent;
	}
	public String getIncomeUnemployment() {
		return incomeUnemployment;
	}
	public void setIncomeUnemployment(String incomeUnemployment) {
		this.incomeUnemployment = incomeUnemployment;
	}
	public String getIncomeOwnBusiness() {
		return incomeOwnBusiness;
	}
	public void setIncomeOwnBusiness(String incomeOwnBusiness) {
		this.incomeOwnBusiness = incomeOwnBusiness;
	}
	public String getIncomeOther() {
		return incomeOther;
	}
	public void setIncomeOther(String incomeOther) {
		this.incomeOther = incomeOther;
	}
	public String getEcName() {
		return ecName;
	}
	public void setEcName(String ecName) {
		this.ecName = ecName;
	}
	public String getEcRelationship() {
		return ecRelationship;
	}
	public void setEcRelationship(String ecRelationship) {
		this.ecRelationship = ecRelationship;
	}
	public String getEcAddress() {
		return ecAddress;
	}
	public void setEcAddress(String ecAddress) {
		this.ecAddress = ecAddress;
	}
	public String getEcMainPhone() {
		return ecMainPhone;
	}
	public void setEcMainPhone(String ecMainPhone) {
		this.ecMainPhone = ecMainPhone;
	}
	public String getEcMobilePhone() {
		return ecMobilePhone;
	}
	public void setEcMobilePhone(String ecMobilePhone) {
		this.ecMobilePhone = ecMobilePhone;
	}
	public String getEcWorkPhone() {
		return ecWorkPhone;
	}
	public void setEcWorkPhone(String ecWorkPhone) {
		this.ecWorkPhone = ecWorkPhone;
	}
	
}
