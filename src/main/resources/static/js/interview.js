$(document).ready(function() {
	
	$('#dobCalendar').calendar({
		type: 'date',
		startMode: 'year'
	});
	
	$('#dobCalendar1').calendar({
		type: 'date',
		startMode: 'year'
	});
	
	$('#appointmentCalendar').calendar();
	
	$('#appointmentCalendar1').calendar();
	
	$('#insuranceExpDate').calendar({
		type: 'month',
		startMode: 'year'
	});
	
	$("#copyAddressDiv").checkbox('setting', 'onChange', function () {
	    var ship = $(this).is(":checked");
	    
	    if (ship) {
		      $("[name='physicalAddress']").val($("[name='postalAddress']").val());
		      $("[name='physicalState']").val($("[name='postalState']").val());
		      $("[name='physicalCountry']").val($("[name='postalCountry']").val());
		      $("[name='physicalZipcode']").val($("[name='postalZipcode']").val());
		}
	});
	
	$("input[name='mainPhone'], " +
		"input[name='mobilePhone'], " +
		"input[name='workPhone'], " +
		"input[name='tutorPhone']," +
		"input[name='ecMainPhone']," +
		"input[name='ecMobilePhone']," +
		"input[name='ecWorkPhone']").keyup(function() {
	    $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d)+$/, "($1) $2-$3"));
	});
	
	$('#scholarity').on('change', function() {
		var selection = $('#scholarity').val();
		console.log(selection);
		
		
		if (selection === "OTHER") {
			$('#otherInputDiv').show();
			$("[name='otherInput']").val("");
		} else {
			$('#otherInputDiv').hide();
			$("[name='otherInput']").val("");
		}
	});
	
	$("input[name='incomeSalary']," +
		"input[name='incomeSocialSecurity'], " +
		"input[name='incomePension']," +
		"input[name='incomeEBT']," +
		"input[name='incomeChildSupport']," +
		"input[name='incomeWellness']," +
		"input[name='incomeRent']," +
		"input[name='incomeUnemployment']," +
		"input[name='incomeOwnBusiness']," +
		"input[name='incomeOther']").bind('keypress', function(e){
			var keyCode = (e.which)?e.which:event.keyCode
			return !(keyCode>31 && (keyCode<48 || keyCode>57)); 
	});
	
	$("input[name='incomeSalary']," +
			"input[name='incomeSocialSecurity'], " +
			"input[name='incomePension']," +
			"input[name='incomeEBT']," +
			"input[name='incomeChildSupport']," +
			"input[name='incomeWellness']," +
			"input[name='incomeRent']," +
			"input[name='incomeUnemployment']," +
			"input[name='incomeOwnBusiness']," +
			"input[name='incomeOther']").keyup(function(event) {
		
		// skip for arrow keys
		if(event.which >= 37 && event.which <= 40) return;
		
		$(this).formatCurrency();
	});
	
	/* $('#medicare').on('change', function() {
		var selection = $('#medicare').val();
		console.log(selection);
		
		
		if (selection === "complementary") {
			$('#compMedicareDiv').show();
			$("[name='compMedicare']").val("");
		} else {
			$('#compMedicareDiv').hide();
			$("[name='compMedicare']").val("");
		}
	}); */
	
	$('#addUpload').click(function(){
		$('#uploadModal').modal('show');    
	});
	
	$('#addEducation').click(function(){
		$('#educationModal').modal('show');    
	});
});